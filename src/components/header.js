import React, { useEffect, useState } from "react"
import { Link } from "gatsby"
import Button from "./elements/Button"
import { FaBars } from "react-icons/fa"
import { ImCross } from "react-icons/im"

const Header = () => {
  const [smallDevice, setSmallDevice] = useState(false);
  const [clicked, setClicked] = useState(false);

  useEffect(() => {
    const resizeHandle = () => {
      if (window.innerWidth <= 900) {
        setSmallDevice(true)
      } else {
        setClicked(false);
        setSmallDevice(false)
      }
    }
    resizeHandle()
    window.addEventListener("resize", resizeHandle)

    return () => window.removeEventListener("resize", resizeHandle)
  }, [])

  return (
    <header className="header">
      <h1 className="header__logo">
        <Link to="/">Travel World</Link>
      </h1>
      {clicked ? (
        <ImCross
          className="header__menu"
          onClick={() => setClicked(!clicked)}
        />
      ) : (
        <FaBars className="header__menu" onClick={() => setClicked(!clicked)} />
      )}

      <ul
        className={`header__nav ${smallDevice && "header__nav--mobile"} ${
          smallDevice && !clicked ? "hide" : ''
        }`}
      >
        <li className="header__nav--item">
          <Link to="/">About</Link>
        </li>
        <li className="header__nav--item">
          <Link to="/">Trips</Link>
        </li>
        <li className="header__nav--item">
          <Link to="/">Careers</Link>
        </li>
        <li className="header__nav--item">
          <Link to="/">Contact</Link>
        </li>
        {smallDevice && (
          <li className="header__nav--item">
            <Button text="Book a Flight" btnStyle="large" to="/" />
          </li>
        )}
      </ul>
      {!smallDevice && <Button text="Book a Flight" btnStyle="large" to="/" />}
    </header>
  )
}

export default Header
