import React from 'react'
import { Link } from "gatsby";

const Button = ({text,handleOnClick, btnStyle, to}) => {
      return (
            <div className={`cta ${btnStyle}`} onClick={handleOnClick}>
                 <Link to={to}>{text}</Link> 
            </div>
      )
}

export default Button
