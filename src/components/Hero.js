import React from "react"
import Button from "./elements/Button"
import BgVideo from "../video/vid1.mp4"

const Hero = () => {
  return (
    <main className="heroSection">
      <video
        src={BgVideo}
        autoPlay
        muted
        loop
        playsInline
        id="bgAutoplay"
        type="video/mp4"
      />
      <div className="heroSection__target">
        <h2>Magnificent Views</h2>
        <span>Of this beautiful world</span>
        <Button text="Travel Now" to="/" btnStyle="large" />
      </div>
    </main>
  )
}

export default Hero
