import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import { GoLocation } from "react-icons/go"
import Button from "./elements/Button"

const getTripsData = data => {
  const tripsArray = []
  data.allTripsJson.edges.forEach(item => {
    tripsArray.push(
      <div className="tripsSection__box" key={item.node.text}>
        <Img
          className="tripsSection__box--image"
          src={item.node.img.childImageSharp.fluid.src}
          fluid={item.node.img.childImageSharp.fluid}
        />
        <div className="tripsSection__box--info">
          <span className="tripsSection__box--txt">
            {" "}
            <GoLocation className="tripsSection__box--icon" /> {item.node.text}
          </span>
          <Button text={item.node.button} to="/" btnStyle="primary" />
        </div>
      </div>
    )
  })
  return tripsArray
}

const Trips = () => {
  const data = useStaticQuery(graphql`
    query TripsQuery {
      allTripsJson {
        edges {
          node {
            altimg
            text
            button
            img {
              childImageSharp {
                fluid {
                  src
                }
              }
            }
          }
        }
      }
    }
  `)
  return (
    <div className="tripsSection">
      <h2>Explore</h2>
      <div className="tripsSection--wrapper">{getTripsData(data)}</div>
    </div>
  )
}

export default Trips
