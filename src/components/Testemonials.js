import { graphql, useStaticQuery } from "gatsby"
import React from "react"
import { BsCheckCircle } from "react-icons/bs"
import { CgRadioChecked } from "react-icons/cg"
import Img from 'gatsby-image';

const Testemonials = () => {
      const data = useStaticQuery(graphql`
      query {
            allFile(
                  filter: {name: {in: ["opinion1", "opinion2"]}, ext: {regex: "/(jpg)|(png)|(jpeg)/"}}
                ) {
                  edges {
                    node {
                      childImageSharp {
                        fluid {
                          src
                        }
                      }
                    }
                  }
                }
            }
      `);
  return (
    <div className="testemonials">
      <h2>Testimonials</h2>
      <h3>What People Say About Us</h3>
      <div className="testemonials__content--left">
        <div className="testemonials__content--box">
          <BsCheckCircle className="testemonials__content--icon blue"/>
          <h4>Will Jansburg</h4>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam. "
          </p>
        </div>
        <div className="testemonials__content--box">
          <CgRadioChecked className="testemonials__content--icon red"/>
          <h4>Joline Franc</h4>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam. "
          </p>
        </div>
      </div>
      <div className="testemonials__content--right"> 
      {data.allFile.edges.map((item, index) => (
            <Img className="testemonials__image" key={index} fluid={item.node.childImageSharp.fluid} />
      ))}
      </div>
    </div>
  )
}

export default Testemonials
