import * as React from "react";
import Header from "./Header";
import '../styles/themes/default/themes.scss';

const Layout = ({ children }) => {
  return (
    <>
      <Header />

      <main className="wrapper">{children}</main>
    </>
  )
}

export default Layout
