import * as React from "react"
import Hero from "../components/Hero"
import Trips from '../components/Trips'
import Testemonials from '../components/Testemonials'
import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <Hero />
    <Trips />
    <Testemonials />
  </Layout>
)

export default IndexPage
